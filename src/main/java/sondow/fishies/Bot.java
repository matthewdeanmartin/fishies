package sondow.fishies;

public class Bot {

    private Bluesky bluesky;
    private Tooter tooter;

    public Bot(Bluesky bluesky, Tooter tooter) {
        this.bluesky = bluesky;
        this.tooter = tooter;
    }

    public Bot() {
        BotConfig botConfig = new BotConfigFactory().configure();
        BlueskyConfig blueskyConfig = botConfig.getBlueskyConfig();
        if (blueskyConfig != null) {
            this.bluesky = new Bluesky(blueskyConfig);
        }
        MastodonConfig mastodonConfig = botConfig.getMastodonConfig();
        if (mastodonConfig != null) {
            this.tooter = new Tooter(mastodonConfig);
        }
    }

    public void go() {
        AquariumBuilder builder = new AquariumBuilder();
        Post post = builder.build();
        if (bluesky != null) {
            bluesky.post(post.getBodyText());
        }
        if (tooter != null) {
            tooter.toot(post);
        }
    }

    public static void main(String[] args) {
        new Bot().go();
    }
}
