package sondow.fishies

import org.junit.Rule
import org.junit.contrib.java.lang.system.EnvironmentVariables
import spock.lang.Specification

class BotConfigFactorySpec extends Specification {

    @Rule
    public final EnvironmentVariables envVars = new EnvironmentVariables()

    def "configure should populate a configuration with environment variables"() {
        setup:
        FileClerk fileClerk = Mock()
        Environment environment = new Environment(new Keymaster(fileClerk))
        String filler = Environment.SPACE_FILLER
        envVars.set("cred_bluesky", "${filler}yaddayadda${filler}")

        when:
        BotConfigFactory factory = new BotConfigFactory(environment)
        BotConfig botConfig = factory.configure()
        BlueskyConfig blueskyConfig = botConfig.blueskyConfig
        println blueskyConfig.appPassword

        then:
        blueskyConfig.appPassword == 'yaddayadda'
        1 * fileClerk.readTextFile('build.properties') >> 'root.project.name=castle'
        1 * fileClerk.readTextFile('castle-encryption-keys.json')
        1 * fileClerk.readTextFile('../cipher/castle-encryption-keys.json')
        1 * fileClerk.readTextFile('castle-values.json')
        1 * fileClerk.readTextFile('../crypt/castle-values.json')
        0 * _._
    }
}
